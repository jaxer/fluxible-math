FROM frozeneye/aarch64-nodejs
WORKDIR /usr/src/app/
ADD package.json package.json
RUN npm install
ADD . .
RUN npm run-script build
