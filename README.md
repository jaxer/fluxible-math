[![build status](https://gitlab.com/jaxer/fluxible-math/badges/master/build.svg)](https://gitlab.com/jaxer/fluxible-math/commits/master)

# fluxible-math

A simple multiplayer game.

Used:
- Fluxible framework (React views), isomorphic
- WebSockets (socket.io) for all communication
- jest for tests

# run

    npm install
    npm run-script build
    npm start
  
# tests

    npm test

# demo

[http://math.sander.ee](http://math.sander.ee)