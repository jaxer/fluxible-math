import Challenge from '../Challenge';
import Operation from '../Operation';

describe('Challenge', () => {
    it('serializes', () => {
        let c = new Challenge();
        c.a = 3;
        c.b = 5;
        c.op = new Operation('+', (a, b) => a);
        c.proposedAnswers = [10, 30, 1, 8];
        expect(c.serialize()).toEqual({
            a: 3,
            answers: [10, 30, 1, 8],
            b: 5,
            op: '+'
        });
    });
});
