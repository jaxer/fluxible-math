import React from 'react';

class Footer extends React.Component {
    render() {
        return (
            <footer className="footer">
                <p>&copy; <a href="mailto:alex@sander.ee">alex</a></p>
            </footer>
        );
    }
}

export default Footer;
